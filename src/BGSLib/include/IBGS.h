#pragma once

#include <cv.h>

class IBGS
{
public:
  virtual void process(const cv::Mat &img_input, cv::Mat &img_output) = 0;
  virtual ~IBGS(){}
  virtual int getNbChannel()=0; 
  virtual void init(const cv::Mat& ref){}

private:
  virtual void saveConfig() = 0;
  virtual void loadConfig() = 0;
};