#ifndef LOBSTER_BGS_H
#define LOBSTER_BGS_H


#include "IBGSStatic.h"

class BackgroundSubtractorLOBSTER;


class LobsterBGS : public IBGSStatic
{

public:
	LobsterBGS();
	virtual ~LobsterBGS();
	void init(const cv::Mat& ref);
	void process(const cv::Mat &img_input, cv::Mat &img_output);
	void removeBlobFromModel(const cv::Mat& labelFrame, const std::set<int> label, const std::vector<cv::Rect>& rectList, const cv::Mat& currentImageHistory);
private:
	void saveConfig();
	void loadConfig();
	virtual int getNbChannel() { return 3; }; 
	BackgroundSubtractorLOBSTER* mBGSSubstractor;

};
#endif