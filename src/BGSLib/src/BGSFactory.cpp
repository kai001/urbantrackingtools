#include "BGSFactory.h"
#include "AdaptiveBackgroundLearning.h"
#include "dp/DPAdaptiveMedianBGS.h"
#include "dp/DPEigenbackgroundBGS.h"
#include "dp/DPGrimsonGMMBGS.h"
#include "dp/DPMeanBGS.h"
#include "dp/DPPratiMediodBGS.h"
#include "dp/DPWrenGABGS.h"
#include "dp/DPZivkovicAGMMBGS.h"
#include "FrameDifferenceBGS.h"
#include "tb/FuzzyChoquetIntegral.h"
#include "tb/FuzzySugenoIntegral.h"

#include "lb/LBAdaptiveSOM.h"
#include "lb/LBFuzzyAdaptiveSOM.h"
#include "lb/LBFuzzyGaussian.h"
#include "lb/LBMixtureOfGaussians.h"
#include "lb/LBSimpleGaussian.h"
#include "MixtureOfGaussianV1BGS.h"
#include "MixtureOfGaussianV2BGS.h"
#include "jmo/MultiLayerBGS.h"
#include "StaticFrameDifferenceBGS.h"
#include "LobsterBGS.h"
#ifdef USEVIBE
#include "VibeBGS.h"
#endif


#include "WeightedMovingMeanBGS.h"
#include "WeightedMovingVarianceBGS.h"
#include "PlaybackBGS.h"
#include <sstream>
BGSFactory::BGSFactory()
{
	nameToInstanceMap["AdaptiveBackgroundLearning"]		= &createInstance<AdaptiveBackgroundLearning>;
	nameToInstanceMap["DPAdaptiveMedianBGS"]			= &createInstance<DPAdaptiveMedianBGS>;
	nameToInstanceMap["DPEigenbackgroundBGS"]			= &createInstance<DPEigenbackgroundBGS>;
	nameToInstanceMap["DPGrimsonGMMBGS"]				= &createInstance<DPGrimsonGMMBGS>;
	nameToInstanceMap["DPMeanBGS"]						= &createInstance<DPMeanBGS>;
	nameToInstanceMap["DPPratiMediodBGS"]				= &createInstance<DPPratiMediodBGS>;
	nameToInstanceMap["DPWrenGABGS"]					= &createInstance<DPWrenGABGS>;
	nameToInstanceMap["DPZivkovicAGMMBGS"]				= &createInstance<DPZivkovicAGMMBGS>;
	nameToInstanceMap["FrameDifferenceBGS"]				= &createInstance<FrameDifferenceBGS>;
	nameToInstanceMap["FuzzyChoquetIntegral"]			= &createInstance<FuzzyChoquetIntegral>;
	nameToInstanceMap["FuzzySugenoIntegral"]			= &createInstance<FuzzySugenoIntegral>;
	nameToInstanceMap["LobsterBGS"]			= &createInstance<LobsterBGS>;
	nameToInstanceMap["LBAdaptiveSOM"]					= &createInstance<LBAdaptiveSOM>;
	nameToInstanceMap["LBFuzzyAdaptiveSOM"]				= &createInstance<LBFuzzyAdaptiveSOM>;
	nameToInstanceMap["LBFuzzyGaussian"]				= &createInstance<LBFuzzyGaussian>;
	nameToInstanceMap["LBMixtureOfGaussians"]			= &createInstance<LBMixtureOfGaussians>;
	nameToInstanceMap["LBSimpleGaussian"]				= &createInstance<LBSimpleGaussian>;
	nameToInstanceMap["MixtureOfGaussianV1BGS"]			= &createInstance<MixtureOfGaussianV1BGS>;
	nameToInstanceMap["MixtureOfGaussianV2BGS"]			= &createInstance<MixtureOfGaussianV2BGS>;
	nameToInstanceMap["MultiLayerBGS"]					= &createInstance<MultiLayerBGS>;
	nameToInstanceMap["StaticFrameDifferenceBGS"]		= &createInstance<StaticFrameDifferenceBGS>;
#ifdef USEVIBE
	nameToInstanceMap["VibeBGS"]						= &createInstance<VibeBGS>;
#endif

	nameToInstanceMap["WeightedMovingMeanBGS"]			= &createInstance<WeightedMovingMeanBGS>;
	nameToInstanceMap["WeightedMovingVarianceBGS"]		= &createInstance<WeightedMovingVarianceBGS>;

	nameToInstanceMap["PlaybackBGS"]		= &createInstance<PlaybackBGS>;
}

BGSFactory::~BGSFactory()
{

}

IBGS* BGSFactory::getBGSInstance(const std::string& bgsMethodName) const
{
	IBGS* bgs = nullptr;

	map_type::const_iterator it = nameToInstanceMap.find(bgsMethodName);
	if(it != nameToInstanceMap.end())
		bgs = it->second();
	else
		std::cout << bgsMethodName << " unavailable. Please choose from the following list:\n" <<  getBGSMethodList();
	return bgs;
}


std::string BGSFactory::getBGSMethodList() const
{
	map_type::const_iterator it;
	std::stringstream ss;
	for(it = nameToInstanceMap.begin(); it != nameToInstanceMap.end(); ++it)
		ss << it->first << "\n";

	return ss.str();
}