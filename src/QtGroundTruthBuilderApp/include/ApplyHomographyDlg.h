#ifndef APPLY_HOMOGRAPHY_DLG_H
#define APPLY_HOMOGRAPHY_DLG_H

#include <qdialog.h>
#include "ui_ApplyHomographyDlg.h"
#include <map>

class AppContext;
class SceneObject;

class ApplyHomographyDlg: public QDialog
{
		Q_OBJECT
public:
		ApplyHomographyDlg(QWidget *parent, AppContext* appContext);
		void run();

private slots:
		void ChooseFileDlg();

private:
	Ui::ApplyHomographyDlg mUi;
	AppContext* mContext;

};

#endif