#ifndef TRAFFIC_INTELLIGENCE_SQL_IMPORTER_H
#define TRAFFIC_INTELLIGENCE_SQL_IMPORTER_H

#include <QString>
#include "GTImporter.h"
#include <vector>
#include <opencv2/opencv.hpp>
class AppContext;

class TrafficIntelligenceSQLImporter: public GTImporter
{
public:
	TrafficIntelligenceSQLImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight);
	virtual ~TrafficIntelligenceSQLImporter();
	void LoadDatabase();
	bool askHomography() { return true;}
private:
	cv::Rect getBoundingBox(const std::vector<cv::Point>& points);


};

#endif