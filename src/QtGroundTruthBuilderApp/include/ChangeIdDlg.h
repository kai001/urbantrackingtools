#ifndef CHANGE_ID_DLG
#define CHANGE_ID_DLG

#include <qdialog.h>
#include "ui_ChangeIdDlg.h"
#include <vector>
class AppContext;
class SceneObject;

class ChangeIdDlg: public QDialog
{
		Q_OBJECT
public:
		ChangeIdDlg(QWidget *parent, AppContext* appContext);
		void run();

private:
	Ui::ChangeIdDlg mUi;
	AppContext* mContext;
	std::map<QListWidgetItem*, SceneObject*> mFromItemToObjectMap;
	std::map<QListWidgetItem*, SceneObject*> mToItemToObjectMap;
};



#endif