#ifndef LUND_IMPORTER_H
#define LUND_IMPORTER_H

#include <QString>
#include "GTImporter.h"

class AppContext;


class LundImporter : public GTImporter
{
public:
	LundImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight);
	
	bool askHomography() { return false;}
	virtual ~LundImporter();
	virtual void LoadDatabase();
private:
	

};


#endif
