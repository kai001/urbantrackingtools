#ifndef OBJECT_OBSERVATION_H
#define OBJECT_OBSERVATION_H


#include <QGraphicsRectItem>
#include <QRectF>
#include <QColor>

class SceneObject;

class ObjectObservation : public QGraphicsRectItem
{
public:
	ObjectObservation(QColor color, bool interpolated);
	virtual ~ObjectObservation();
	enum Corner
	{
		TOPLEFTHANDLE,
		TOPHANDLE,
		TOPRIGHTHANDLE,
		RIGHTHANDLE,
		BOTTOMLEFTHANDLE,
		BOTTOMHANDLE,
		BOTTOMRIGHTHANDLE,
		LEFTHANDLE
	};
	void associateToSceneObject(SceneObject* object);
	SceneObject* getSceneObject() {return mObject; }
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,  QWidget *widget);
	virtual void hoverMoveEvent(QGraphicsSceneHoverEvent *e);
	virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *e);
	virtual QRectF boundingRect() const;
	bool hasClickedOn(QPointF p1, QPointF p2);
	void mousePressEvent(QGraphicsSceneMouseEvent *e);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *e);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *e);
	void redrawItem();
	void setBoundingBox(QPointF topLeftCorner, QPointF bottomRightCorner);
	void setColor(unsigned int R, unsigned int G, unsigned int B) {mColor.setRed(R); mColor.setGreen(G); mColor.setBlue(B); }
	QRectF getRectangle(const QPointF& p1, const QPointF& p2) const;
	bool isInterpolated() const {return mInterpolated;}




private:
	bool mInterpolated;
	SceneObject* mObject;
	int mHoverPoint;
	int mSelectedPoint;
	QPointF mHandleSize;
	QColor mColor;
	QPointF mLastPoint;
	
};


#endif