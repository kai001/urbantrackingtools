#ifndef CAVIAR_IMPORTER_H
#define CAVIAR_IMPORTER_H

#include <QString>
#include <map>
#include "GTImporter.h"
class AppContext;
class CaviarImporter: public GTImporter
{
	public:
	CaviarImporter(StatusProgressBar* progressBarControl, 	AppContext* appContext,unsigned int videoWidth, unsigned int videoHeight);	
	virtual ~CaviarImporter();

	bool askHomography() { return false;}
	virtual void LoadDatabase();


private:



};


#endif


