#ifndef TRAFFIC_INTELLIGENCE_GT_IMPORTER_H
#define TRAFFIC_INTELLIGENCE_GT_IMPORTER_H


#include <QString>
#include "GTImporter.h"


class AppContext;



class TrafficIntelligenceGTImporter : public GTImporter
{
public:
	TrafficIntelligenceGTImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight);
	virtual ~TrafficIntelligenceGTImporter();
	virtual void LoadDatabase();
	bool askHomography() {return false;}

private:

};



#endif