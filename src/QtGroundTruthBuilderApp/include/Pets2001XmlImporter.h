#ifndef PETS2001_XML_IMPORTER_H
#define PETS2001_XML_IMPORTER_H

#include <QString>
#include <map>
#include "GTImporter.h"

class AppContext;

class Pets2001XmlImporter: public GTImporter
{
public:
	Pets2001XmlImporter(StatusProgressBar* progressBarControl, AppContext* appContext,unsigned int videoWidth, unsigned int videoHeight);	
	virtual ~Pets2001XmlImporter();

	
	bool askHomography() { return false;}
	virtual void LoadDatabase();


private:
	void LoadFile(QString fileName);


};

#endif