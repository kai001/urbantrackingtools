#ifndef DELETE_RANGE_DLG
#define DELETE_RANGE_DLG

#include <qdialog.h>
#include "ui_DeleteRangeDlg.h"
#include <map>
#include "ProgressDialog.h"

class AppContext;
class SceneObject;

class DeleteRangeDlg: public ProgressDialog
{
		Q_OBJECT
public:
		DeleteRangeDlg(QWidget *parent, StatusProgressBar* progressBarControl, AppContext* appContext);
		virtual BaseTask* getBaseTask();

private:
	Ui::DeleteRangeDlg mUi;
	AppContext* mContext;
	std::map<QListWidgetItem*, SceneObject*> mItemToObjectMap;
};

#endif