#include "DeleteRangeDlg.h"
#include "AppContext.h"
#include "SceneObject.h"
#include <QString>
#include "ObjectObservation.h"
#include "DrawableSceneView.h"
#include <QListWidgetItem>
#include "DeleteRangeTask.h"

DeleteRangeDlg::DeleteRangeDlg(QWidget *parent, StatusProgressBar* progressBarControl, AppContext* appContext)
: ProgressDialog(progressBarControl, parent) 
, mContext(appContext)
{
	mUi.setupUi(this);
	mUi.objectList->setSelectionMode(QAbstractItemView::MultiSelection);
	const std::vector<SceneObject*>& objectList =  mContext->getSceneObjectList();
	for(auto it = objectList.begin(); it != objectList.end(); ++it)
	{
		QListWidgetItem* item = new QListWidgetItem(QString(((*it)->getId()+" "+(*it)->getDescription()).c_str()));
		mItemToObjectMap[item] = *it;
		mUi.objectList->addItem(item);
	}
}

 BaseTask* DeleteRangeDlg::getBaseTask()
 {
	 std::set<SceneObject*> toDeleteSet;
	 auto selectedItemList = mUi.objectList->selectedItems();
	for(auto itemIt = selectedItemList.begin(); itemIt != selectedItemList.end(); ++itemIt)	
		toDeleteSet.insert(mItemToObjectMap[(*itemIt)]);
	
	int startTime, endTime;
	bool allTimestamp = mUi.checkBoxAll->isChecked();
	if(!allTimestamp)
	{
		startTime= mUi.fromTimestamp->value();
		endTime = mUi.toTimestamp->value();
	}
	 return new DeleteRangeTask(getStatusBarProgressControl(), mContext,mItemToObjectMap, toDeleteSet, startTime, endTime, allTimestamp);
 }
