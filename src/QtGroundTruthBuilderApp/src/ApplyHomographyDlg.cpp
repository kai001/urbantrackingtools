#include "ApplyHomographyDlg.h"
#include <QFileDialog>
#include "MatrixIO.h"
#include <QString>
#include "AppContext.h"
#include <map>
#include "DrawableSceneView.h"
#include "ObjectObservation.h"
#include <Logger.h>

ApplyHomographyDlg::ApplyHomographyDlg(QWidget *parent, AppContext* appContext)
: QDialog(parent) 
, mContext(appContext)
{
	mUi.setupUi(this);
	bool connected = connect(mUi.openFileBtnDlg, SIGNAL(clicked()), this, SLOT(ChooseFileDlg()));
}

void ApplyHomographyDlg::run()
{
	QString filePath = mUi.filePathTxtBox->text();
	cv::Mat homographyMatrix;
	bool success = Utils::IO::LoadMatrix<float>(filePath.toStdString(), homographyMatrix);
	if(success)
	{
		
		if(mUi.transposeMatrixCb->isChecked())
			cv::transpose(homographyMatrix, homographyMatrix);
		if( mUi.invertHomographyCb->isChecked())
			cv::invert(homographyMatrix,homographyMatrix);

		auto& timestampMap = mContext->getTimestampToScene();
		int nbBox = 0;
		for(auto itTimestamp = timestampMap.begin(); itTimestamp != timestampMap.end(); ++itTimestamp)
		{
			DrawableSceneView* sceneView =itTimestamp->second;
			if(sceneView)
			{
				QList<QGraphicsItem*> items = sceneView->items();
				for (int i = 0; i < items.size(); ++i)
				{
					ObjectObservation* obj = dynamic_cast<ObjectObservation*>(items[i]);
					if(obj)
					{
						QRectF rect = obj->rect();
						cv::Point2f topLeftCorner(rect.x(), rect.y());
						cv::Point2f bottomRightCorner(rect.x() + rect.width(), rect.y() + rect.height());
						std::vector<cv::Point2f> beforeHomography;
						beforeHomography.push_back(topLeftCorner);
						beforeHomography.push_back(bottomRightCorner);
						std::vector<cv::Point2f> afterHomography;
						cv::perspectiveTransform(beforeHomography,afterHomography, homographyMatrix);
						cv::Point2f correctedTopLeftCorner = afterHomography[0];
						cv::Point2f correctedBottomRightCorner = afterHomography[1];
						obj->setRect(correctedTopLeftCorner.x, correctedTopLeftCorner.y,  correctedBottomRightCorner.x-correctedTopLeftCorner.x, correctedBottomRightCorner.y-correctedTopLeftCorner.y);
						++nbBox;
					}
				}
			}
		}
		LOGINFO("Homography applied to " << nbBox << " bounding box");
	}
}


void ApplyHomographyDlg::ChooseFileDlg()
{
	QString fileName = QFileDialog::getOpenFileName(this,
		tr("Load homography file"), "",
		tr("All Files (*)"));
	if(fileName != "")
	{
		mUi.filePathTxtBox->setText(fileName);
	}
}