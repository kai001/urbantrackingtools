#include "BoundingBox.h"
#include "BoundingBoxHandler.h"

BoundingBox::BoundingBox(unsigned int x0, unsigned int x1, unsigned int y0, unsigned int y1, cv::Scalar color, unsigned int thickness)
: mColor(color)
, mThickness(thickness)
, mX0(x0)
, mX1(x1)
, mY0(y0)
, mY1(y1)
, mSelected(false)
, mHandler(nullptr)
{
	
}

BoundingBox::~BoundingBox()
{
	delete mHandler;
}

void BoundingBox::draw(cv::Mat& background, cv::Scalar color, unsigned int thickness)
{
	if(!mHandler)
		mHandler = new BoundingBoxHandler(this);

	unsigned int xSz = mX1- mX0;
	unsigned int ySz = mY1- mY0;


	cv::rectangle(background, cv::Rect(mX0,mY0, xSz, ySz), color, thickness);
	if(mSelected)
	{
		mHandler->draw(background);
	}
	//if highligthed
}


void BoundingBox::draw(cv::Mat& background)
{
	draw(background, mColor, mThickness);
}



