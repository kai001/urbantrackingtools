#include "LundImporter.h"
#include <fstream>
#include <string>
#include "StringHelpers.h"
#include "SceneObject.h"
#include <assert.h>
#include <map>
#include "DrawableSceneView.h"
#include "AppContext.h"
#include "ObjectObservation.h"

LundImporter::LundImporter(StatusProgressBar* progressBarControl, AppContext* appContext, unsigned int videoWidth, unsigned int videoHeight)
: GTImporter(progressBarControl,appContext, "LundImporter", "*.txt", "LUND", GTImporter::FILE, videoWidth, videoHeight)
{

}

LundImporter::~LundImporter()
{

}

void LundImporter::LoadDatabase()
{
	const QString& path = getLoadPath();
	//Lire fichier ligne par ligne
	std::ifstream f(path.toStdString().c_str());
	AppContext* context = getAppContext();
	unsigned int id = 0;
	std::string newLine;
	std::string currentType("");
	SceneObject* currentSceneObj = nullptr;
	QColor currentColor; 
	std::map<unsigned int, DrawableSceneView*>& sceneMap = getAppContext()->getTimestampToScene();
	updatePercent(10);
	while(std::getline( f, newLine ))
	{
		if(Utils::String::BeginsWith(newLine, "Type:"))
		{
			currentType = Utils::String::EnleveEspaceDebutFin(Utils::String::RemoveString(newLine, "Type:"));
			std::string idStr = Utils::String::toString(id);
			currentSceneObj = new SceneObject(idStr, currentType+ " " + idStr, currentType);
			getAppContext()->getSceneObjectList().push_back(currentSceneObj);
			getAppContext()->getObjectTypeManager().addType(currentType);
			currentColor = QColor(rand()%255,rand()%255, rand()%255);
			++id;
			//New object
		}
		else if(Utils::String::Contains(newLine, ";"))
		{
			StringArray obsData = Utils::String::Split(newLine, ";");
			if(obsData.size() == 19)
			{
				unsigned int timestamp;
				if(Utils::String::StringToType(obsData[0], timestamp))
				{
					auto it = sceneMap.find(timestamp);
					if(it == sceneMap.end())
					{
						auto itPair = sceneMap.insert(std::pair<unsigned int, DrawableSceneView*>(timestamp, new DrawableSceneView(timestamp, nullptr, getAppContext())));
						it = itPair.first;
					}
					DrawableSceneView* currentScene = it->second;
					ObjectObservation* objObs = new ObjectObservation(currentColor, false);
					std::vector<unsigned int> Xarray, Yarray;
					for(unsigned int i = 3; i < 17; i = i+2)
					{
						unsigned int X, Y;
						bool success = Utils::String::StringToType(obsData[i], X) && Utils::String::StringToType(obsData[i+1], Y);
						if(success)
						{
							Xarray.push_back(X);
							Yarray.push_back(Y);
						}
						else
							assert(false);
					}
					unsigned int XMin = *std::min_element(Xarray.begin(), Xarray.end());
					unsigned int XMax = *std::max_element(Xarray.begin(), Xarray.end());
					unsigned int YMin = *std::min_element(Yarray.begin(), Yarray.end());
					unsigned int YMax = *std::max_element(Yarray.begin(), Yarray.end());				
					objObs->setRect(QRectF(XMin, YMin, XMax-XMin+1, YMax-YMin+1));					
					objObs->associateToSceneObject(currentSceneObj);
					currentScene->addItem(objObs);
				}
			}
			else
			{
				assert(false);
			}
		}
	}
}