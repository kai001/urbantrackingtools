#include "ExporterTask.h"
#include "StatusProgressBar.h"
#include "AppContext.h"
#include "SceneObject.h"
#include <set>
#include "ObjectObservation.h"
#include "StringHelpers.h"
#include "DrawableSceneView.h"

ExporterTask::ExporterTask(StatusProgressBar* progressBarControl, AppContext* context, const QString& name, const QString& extension)
: BaseTask(progressBarControl, "Exporting as " + name)
, mContext(context)
, mName(name)
, mFilePath("")
, mWidth(0)
, mHeight(0)
, mExtension(extension)
{
}


ExporterTask::~ExporterTask() 
{
}

void ExporterTask::cleanObjectsId()
{
	std::set<SceneObject*> usedObject;
	std::map<unsigned int, DrawableSceneView*>& timestampToScene= mContext->getTimestampToScene();
	int newId = 0;
	for (auto it = timestampToScene.begin(); it != timestampToScene.end();++it)
	{		
		unsigned int timestamp = (*it).first;
		DrawableSceneView* sceneView = (*it).second;
		if(sceneView)
		{
			QList<QGraphicsItem *> items = sceneView->items();
			for (int i = 0; i < items.size(); ++i)
			{
				ObjectObservation* obj = dynamic_cast<ObjectObservation*>(items[i]);
				if(obj && obj->getSceneObject())
				{
					if(usedObject.find(obj->getSceneObject()) == usedObject.end())
					{					
						obj->getSceneObject()->setId(Utils::String::toString(newId++));
						usedObject.insert(obj->getSceneObject());
					}
				}
			}
		}
	}
	std::vector<SceneObject*>& objects = mContext->getSceneObjectList();
	for(auto it = objects.begin(); it != objects.end();)
	{
		auto objectIt = usedObject.find(*it);
		if(objectIt == usedObject.end())
			it = objects.erase(it);
		else
			 ++it;
	}
}
