#include "NegativeSampleDlg.h"


NegativeSampleDlg::NegativeSampleDlg(QWidget *parent)
: QDialog(parent) 
{
	mUi.setupUi(this);
	mUi.minX->setValidator(new QIntValidator(this));
	mUi.maxX->setValidator(new QIntValidator(this));
	mUi.minY->setValidator(new QIntValidator(this));
	mUi.maxY->setValidator(new QIntValidator(this));
	mUi.nbSamples->setValidator(new QIntValidator(this));
	setModal(true);
}

void NegativeSampleDlg::getValues(int& minX, int& maxX, int& minY, int& maxY, int& nbSamples)
{
	minX = mUi.minX->text().toInt();
	minY = mUi.minY->text().toInt();
	maxX = mUi.maxX->text().toInt();
	maxY = mUi.maxY->text().toInt();
	nbSamples = mUi.nbSamples->text().toInt();
}




//		void LaunchExtraction(int minX, int maxX, int minY, int maxY, int nbSamples);
