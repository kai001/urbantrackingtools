#ifndef METRICS_HELPERS_H
#define METRICS_HELPERS_H

#include "MeasureDistType.h"
#include <vector>

struct Observation;

namespace MetricsHelpers
{
	bool temporalOverlap(const std::vector<Observation*>& A, const std::vector<Observation*>& B,  int& startOverlap,  int& endOverlap, int& temporalIntersection, int& temporalUnion);
	double spatialOverlap(const Observation* A, const Observation* B);
	Observation* getObservation(const std::vector<Observation*>& observations, unsigned int timestamp);
	double calculateDistance(Observation* obs1, Observation* obs2, MeasureDistType distType);
	double calculateDistanceAbsolute(Observation* obs1, Observation* obs2);
	double calculateDistanceOverlap(Observation* obs1, Observation* obs2);
};



#endif