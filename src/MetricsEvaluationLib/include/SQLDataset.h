#ifndef SQLDataset_H
#define SQLDataset_H


#include <map>
#include <vector>
#include "Dataset.h"
#include "ObjectTypeManager.h"
#include <opencv2/opencv.hpp>

class SceneObject;
class SQLiteManager;
struct Observation;
struct SceneObjectWithObservation;

class SQLDataset : public Dataset
{
public:
	SQLDataset();
	~SQLDataset();
	void setHomography(const cv::Mat& homography) { mHomography = homography;}
	bool Load(const std::string& databaseFile);
private:

	
	bool LoadObjectTypeTable();
	bool LoadObjectsTable();
	bool LoadBoundingBoxesTable();
	bool LoadPointsTable();
	std::map<std::string, SceneObjectWithObservation*> mObjectIdToObjectMap;
	
	cv::Rect getBoundingBox(const std::vector<cv::Point>& points);
	SQLiteManager* mSQL;
	cv::Mat mHomography;	
};


#endif