#ifndef ASSOCIATION_H
#define ASSOCIATION_H

struct SceneObjectWithObservation;

class Association
{
public:
	Association(SceneObjectWithObservation* gt, SceneObjectWithObservation* system, double spatialOverlap, double tempOverlap)
	: mGT(gt)
	, mSystem(system)
	, mSpatialOverlap(spatialOverlap)
	, mTemporalOverlap(tempOverlap)
	{

	}

	SceneObjectWithObservation* getGroundTruth() const  { return mGT; }
	SceneObjectWithObservation* getSystem() const  { return mSystem; }
	double getSpatialOverlap() const { return mSpatialOverlap; }
	double getTemporalOverlap() const { return mTemporalOverlap; }
private:
	SceneObjectWithObservation* mGT;
	SceneObjectWithObservation* mSystem;
	double mSpatialOverlap;
	double mTemporalOverlap;
};

#endif