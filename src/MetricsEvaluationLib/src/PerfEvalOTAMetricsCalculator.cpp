#include "PerfEvalOTAMetricsCalculator.h"
#include <math.h>
#include <iostream>
#include "Observation.h"
#include <vector>
#include "MetricsHelpers.h"
#include "Dataset.h"

PerfEvalOTAMetricsCalculator::PerfEvalOTAMetricsCalculator(const Dataset* groundTruth, const Dataset* experimentalPath, double temporalOverlap, double spatialOverlap)
: mT_ov(spatialOverlap)
, mTR_ov(temporalOverlap) //As defined in the paper, can be adjusted 0.15
, mDataAssociator(nullptr)
, mGroundTruth(groundTruth)
, mSystem(experimentalPath)
{

	if(mGroundTruth->isLoaded() && mSystem->isLoaded())
	{
		auto gtObjectList = mGroundTruth->getSceneObjectList();
		auto systemObjectList = mSystem->getSceneObjectList();
		mDataAssociator = new GTSystemDataAssociator(&gtObjectList, &systemObjectList, mTR_ov, mT_ov);
	}
}


PerfEvalOTAMetricsCalculator::~PerfEvalOTAMetricsCalculator()
{
	delete mDataAssociator;
}


unsigned int PerfEvalOTAMetricsCalculator::getNbGTTracks()
{
	return mGroundTruth->getSceneObjectList().size();
}

unsigned int PerfEvalOTAMetricsCalculator::getNbSystemTracks()
{
	return mSystem->getSceneObjectList().size();
}

//This is kind of redundant with the DataAssociator
unsigned int PerfEvalOTAMetricsCalculator::getCDT()
{
	const auto& gtObjectList = mGroundTruth->getSceneObjectList();
	const auto& systemObjectList = mSystem->getSceneObjectList();
	const auto& systemToGtMapAssociation = mDataAssociator->getSystemToGTMap();
	unsigned int CDT = 0;
	for(unsigned int i = 0; i < gtObjectList.size(); ++i)
	{
		bool truePositive = false;
		SceneObjectWithObservation* gtObj = gtObjectList[i];
		const std::vector<Observation*>& gtObservations = gtObj->mObservations;
		if(!gtObservations.empty())
		{		
			for(unsigned int j = 0; j < systemObjectList.size(); ++j)
			{
				SceneObjectWithObservation* systemObj = systemObjectList[j];
				const std::vector<Observation*>& systemObservations = systemObj->mObservations;
				int startOverlap, endOverlap, temporalIntersection, temporalUnion;
				if(MetricsHelpers::temporalOverlap(gtObservations, systemObservations, startOverlap, endOverlap, temporalIntersection, temporalUnion))
				{
					if(((double)temporalIntersection) / ((double)(gtObservations.size())) >= mTR_ov)//Temporal overlap criterion, Condition 1
					{
						double accumulator = 0;
						//Condition 2, Spatial overlap
						for(int currentTimestamp = startOverlap; currentTimestamp <= endOverlap; ++currentTimestamp)
						{
							Observation* gtObs = MetricsHelpers::getObservation(gtObservations, currentTimestamp);
							Observation* sysObs = MetricsHelpers::getObservation(systemObservations, currentTimestamp);
							if(gtObs && sysObs)							
								accumulator += MetricsHelpers::spatialOverlap(gtObs, sysObs);							
						}
						if(accumulator/(temporalIntersection) >= mT_ov)	
						{
							//This doesn't support id change, but it avoid using 1 system object for 2 GT object
							const auto& sys = systemToGtMapAssociation.find(systemObj);
							if(sys->second == gtObj)
								truePositive = true;	
						}
					}
				}
			}
		}
		else
		{
			std::cout << "Error: No ground truth observation for object" << std::endl;
		}
		CDT += truePositive;
	}
	return CDT;
}
//TODO: Trouver si il y a une relation avec getCDT() et simplifier.
unsigned int PerfEvalOTAMetricsCalculator::getFAT()
{
	return getWrongDetectionScore(mSystem->getSceneObjectList(), mGroundTruth->getSceneObjectList());
}



unsigned int PerfEvalOTAMetricsCalculator::getTDF()
{
	return getWrongDetectionScore(mGroundTruth->getSceneObjectList(), mSystem->getSceneObjectList());
}

unsigned int PerfEvalOTAMetricsCalculator::getTF()
{
	const auto& GTtoSysMap = mDataAssociator->getGTToSystemMap();
	//
	//Si je comprends bien, il faut mettre le nombre d'objets assign�s pour chaque GT lors du calcul du TP. Le probl�me, 
	//c'est qu'il peut y avoir de la fausse fragment si 2 objets se croise longtemps m�me si ils sont
	//Parfaitement bien track� je crois... Il faudrait limiter � 1 le nombre de GT auquel un ST peut �tre associ�. Cela �viterait ce genre de probl�me.... et donnerait un score plus coh�rent pour TP aussi...
	//Puisque la d�finition n'est pas clair, nous feront la somme des fragmentation de tous les objets
	int nbFragmentation = 0;
	for(auto it = GTtoSysMap.begin(); it != GTtoSysMap.end(); ++it)
	{
		if((*it).second.size() > 1)
			nbFragmentation+=(*it).second.size();
	}

	return nbFragmentation;
}

unsigned int PerfEvalOTAMetricsCalculator::getIDC()
{
	unsigned int IDC = 0;
	const auto& gtObjectList = mGroundTruth->getSceneObjectList();
	const auto& systemObjectList = mSystem->getSceneObjectList();
	//For each System track j
	for(unsigned int j = 0; j < systemObjectList.size(); ++j)
	{
		int currentGTId = -1;
		SceneObjectWithObservation* systemObj = systemObjectList[j];
		const std::vector<Observation*>& systemObservations = systemObj->mObservations;
		int IDCj = -1;
		//For every frame k
		for(unsigned int k = 0; k < systemObservations.size(); ++k)
		{
			//We calculate this only for frame with no occlusion, so we want to use NDj,k = 1;
			auto timestamp = systemObservations[k]->mTimestamp;
			Observation* sysObs = MetricsHelpers::getObservation(systemObservations, timestamp);
			if(sysObs)
			{
				unsigned int NDjk = 0;
				int newcurrentGTId = -1;
				for(unsigned int i = 0; i < gtObjectList.size(); ++i)
				{					
					SceneObjectWithObservation* gtObj = gtObjectList[i];
					const std::vector<Observation*>& gtObservations = gtObj->mObservations;
					if(!gtObservations.empty())
					{	
						Observation* gtObs =MetricsHelpers::getObservation(gtObservations, timestamp);
						if(gtObs)
						{
							if((MetricsHelpers::spatialOverlap(sysObs, gtObs) > mT_ov))
							{
								++NDjk;
								newcurrentGTId = i;
							}
						}
					}
				}
				if(NDjk == 1)
				{
					if(IDCj == -1 || newcurrentGTId != currentGTId)
					{
						++IDCj;
						currentGTId = newcurrentGTId;
					}
				}				
			}
		}
		if(IDCj < 0)
			IDCj = 0;
		IDC += IDCj;
	}
	return IDC;
}

double PerfEvalOTAMetricsCalculator::getLT()
{

	const auto& GTtoSysMap = mDataAssociator->getGTToSystemMap();
	unsigned nbObject = 0;
	int latencyAccumulator = 0;
	for(auto it = GTtoSysMap.begin(); it != GTtoSysMap.end(); ++it)
	{
		if(!it->first->mObservations.empty()) // TODO: FIltr� �a � l'ouverture pour �viter d'avoir � surveiller tout le temps la condition)
		{
			int gtTimestamp = it->first->mObservations[0]->mTimestamp;
			unsigned int bestSysTimestamp = -1;
			const auto& associationList = (*it).second;
			for(auto association = associationList.begin(); association != associationList.end(); ++association)
			{		
				if(!(*association)->mObservations.empty())
				{
					unsigned int sysTimestamp = (*association)->mObservations[0]->mTimestamp;
					if(bestSysTimestamp > sysTimestamp)
						bestSysTimestamp = sysTimestamp;
				}

			}
			if(!associationList.empty())
			{
				int latency = bestSysTimestamp-gtTimestamp;
				if(latency < 0)
				{
					//std::cout << "Object was detected in system before GT. Some more work should be done on the GT for object: " << it->first->mSceneObject->getId() << " " << it->first->mSceneObject->getDescription() << std::endl; 
					++nbObject;
				}
				else
				{
					latencyAccumulator += latency;
					++nbObject;
				}
			}
		}
	}


	
	//Le probl�me ici, c'est que la m�thode n'explique absolument pas comment faire lorsqu'il y a plus d'un d'un GT ou plus qu'une association...
	//On va donc faire un average Latency. Il va falloir expliquer les modifications apport�s au m�triques dans le papier


	

	
	return nbObject > 0 ? (double) latencyAccumulator / (double) nbObject : 0 ;
}

double PerfEvalOTAMetricsCalculator::getCTM()
{
	const auto& systemToGtMapAssociation = mDataAssociator->getSystemToGTMap();
	double spatialOverlapSum = 0;
	unsigned int nbOverlap = 0;
	for(auto it = systemToGtMapAssociation.begin(); it != systemToGtMapAssociation.end(); ++it)
	{
		//For each system track, we will compare with a new GT track
		SceneObjectWithObservation* systemObj = (*it).first;
		SceneObjectWithObservation* gtObj = (*it).second;
		if(gtObj &&systemObj)
		{
			//For overlap, we will calculate A(GT, ST)
			int startOverlap, endOverlap, temporalIntersection, temporalUnion;
			if(MetricsHelpers::temporalOverlap(gtObj->mObservations, systemObj->mObservations, startOverlap, endOverlap, temporalIntersection, temporalUnion))
			{
				for(int currentTimestamp = startOverlap; currentTimestamp <= endOverlap; ++currentTimestamp)
				{
					Observation* gtObs = MetricsHelpers::getObservation(gtObj->mObservations, currentTimestamp);
					Observation* sysObs = MetricsHelpers::getObservation(systemObj->mObservations, currentTimestamp);
					if(gtObs && sysObs)			
					{
						spatialOverlapSum +=MetricsHelpers::spatialOverlap(gtObs, sysObs);	
						++nbOverlap;
					}
				}
			}
		}
		
	}
		
	return nbOverlap > 0 ? (double) spatialOverlapSum / (double) nbOverlap : 0 ;
}

double PerfEvalOTAMetricsCalculator::getCTD()
{
	const auto& systemToGtMapAssociation = mDataAssociator->getSystemToGTMap();
	double accumulatorStd = 0;
	double nbOverlap = 0;
	for(auto it = systemToGtMapAssociation.begin(); it != systemToGtMapAssociation.end(); ++it)
	{
		//For each system track, we will compare with a new GT track
		SceneObjectWithObservation* systemObj = (*it).first;
		SceneObjectWithObservation* gtObj = (*it).second;
		//For overlap, we will calculate A(GT, ST)
		int startOverlap, endOverlap, temporalIntersection, temporalUnion;
		if(gtObj && MetricsHelpers::temporalOverlap(gtObj->mObservations, systemObj->mObservations, startOverlap, endOverlap, temporalIntersection, temporalUnion))
		{
			//double overlap = 0;
			std::vector<double> CTList;
			for(int currentTimestamp = startOverlap; currentTimestamp <= endOverlap; ++currentTimestamp)
			{
				Observation* gtObs = MetricsHelpers::getObservation(gtObj->mObservations, currentTimestamp);
				Observation* sysObs = MetricsHelpers::getObservation(systemObj->mObservations, currentTimestamp);
				if(gtObs && sysObs)			
				{
					CTList.push_back(MetricsHelpers::spatialOverlap(gtObs, sysObs));	
				}
			}
			//calculate average
			double avg = 0;
			for(unsigned int i = 0; i < CTList.size(); ++i)
				avg+=CTList[i];
			avg = avg/CTList.size();
			double standardDeviation = 0;
			for(unsigned int i = 0; i < CTList.size(); ++i)
				standardDeviation += pow(CTList[i]-avg, 2);

			standardDeviation = sqrt(standardDeviation/CTList.size());
			accumulatorStd += standardDeviation*CTList.size();
			nbOverlap+=CTList.size();
		}
	}
	return nbOverlap > 0 ? (double) accumulatorStd / (double) nbOverlap : 0 ;
}

double PerfEvalOTAMetricsCalculator::getTMEMT()
{
	const auto& systemToGtMapAssociation = mDataAssociator->getSystemToGTMap();
	double accumulatorAvg = 0;
	double nbOverlap = 0;
	for(auto it = systemToGtMapAssociation.begin(); it != systemToGtMapAssociation.end(); ++it)
	{
		//For each system track, we will compare with a new GT track
		SceneObjectWithObservation* systemObj = (*it).first;
		SceneObjectWithObservation* gtObj = (*it).second;
		//For overlap, we will calculate A(GT, ST)
		
		int startOverlap, endOverlap, temporalIntersection, temporalUnion;
		if(gtObj && MetricsHelpers::temporalOverlap(gtObj->mObservations, systemObj->mObservations, startOverlap, endOverlap, temporalIntersection, temporalUnion))
		{
			std::vector<double> distList;
			for(int currentTimestamp = startOverlap; currentTimestamp <= endOverlap; ++currentTimestamp)
			{
				Observation* gtObs = MetricsHelpers::getObservation(gtObj->mObservations, currentTimestamp);
				Observation* sysObs = MetricsHelpers::getObservation(systemObj->mObservations, currentTimestamp);
				if(gtObs && sysObs)			
				{
					distList.push_back(Rect::dist(gtObs->mRectangle, sysObs->mRectangle));
				}
			}

			//calculate average
			double TME = 0;
			for(unsigned int i = 0; i < distList.size(); ++i)
				TME+=distList[i];
			TME = TME/distList.size();
		
			accumulatorAvg+= TME*distList.size();
			nbOverlap+=distList.size();
		}
	}

	return nbOverlap > 0 ? accumulatorAvg/nbOverlap : 0;
}

double PerfEvalOTAMetricsCalculator::getTMEMTD()
{
	const auto& systemToGtMapAssociation = mDataAssociator->getSystemToGTMap();
	double accumulatorStd = 0;
	double nbOverlap = 0;
	for(auto it = systemToGtMapAssociation.begin(); it != systemToGtMapAssociation.end(); ++it)
	{
		//For each system track, we will compare with a new GT track
		SceneObjectWithObservation* systemObj = (*it).first;
		SceneObjectWithObservation* gtObj = (*it).second;
		//For overlap, we will calculate A(GT, ST)

		int startOverlap, endOverlap, temporalIntersection, temporalUnion;
		if(gtObj && MetricsHelpers::temporalOverlap(gtObj->mObservations, systemObj->mObservations, startOverlap, endOverlap, temporalIntersection, temporalUnion))
		{
			std::vector<double> distList;
			for(int currentTimestamp = startOverlap; currentTimestamp <= endOverlap; ++currentTimestamp)
			{
				Observation* gtObs = MetricsHelpers::getObservation(gtObj->mObservations, currentTimestamp);
				Observation* sysObs = MetricsHelpers::getObservation(systemObj->mObservations, currentTimestamp);
				if(gtObs && sysObs)			
				{
					distList.push_back(Rect::dist(gtObs->mRectangle, sysObs->mRectangle));
				}
			}

			//calculate average
			double TME = 0;
			for(unsigned int i = 0; i < distList.size(); ++i)
				TME+=distList[i];
			TME = TME/distList.size();
			double TMED = 0;
			for(unsigned int i = 0; i < distList.size(); ++i)
				TMED += pow(distList[i]-TME, 2);

			TMED = sqrt(TMED/distList.size());
			accumulatorStd+= TMED*distList.size();
			nbOverlap+=distList.size();
		}
	}
	return nbOverlap > 0 ? (double) accumulatorStd / (double) nbOverlap : 0 ;
}

double PerfEvalOTAMetricsCalculator::getTCM()
{
	int numberOfMatchedTracks = 0;
	const auto& GTtoSysMap = mDataAssociator->getGTToSystemMap();
	double TCsum = 0;
	for(auto currentGtIt = GTtoSysMap.begin(); currentGtIt != GTtoSysMap.end(); ++currentGtIt)
	{
		bool matchingTrack = false;
		SceneObjectWithObservation* gtObj = currentGtIt->first;
		std::list<SceneObjectWithObservation*> currentSystemList = currentGtIt->second;

		double nbGTObservation = gtObj->mObservations.size();
		double maxTC = 0;
		for(auto currentSystemIt = currentSystemList.begin(); currentSystemIt != currentSystemList.end(); ++currentSystemIt)
		{
			double TC = 0;
			SceneObjectWithObservation* systemObj = (*currentSystemIt);
			int startOverlap, endOverlap, temporalIntersection, temporalUnion;
			if(MetricsHelpers::temporalOverlap(gtObj->mObservations, systemObj->mObservations, startOverlap, endOverlap, temporalIntersection, temporalUnion))
			{
				//std::vector<double> distList;
				for(int currentTimestamp = startOverlap; currentTimestamp <= endOverlap; ++currentTimestamp)
				{
					Observation* gtObs = MetricsHelpers::getObservation(gtObj->mObservations, currentTimestamp);
					Observation* sysObs = MetricsHelpers::getObservation(systemObj->mObservations, currentTimestamp);
					
					if(gtObs && sysObs)			
					{
						if(Rect::overlap(gtObs->mRectangle, sysObs->mRectangle) > mT_ov)
						{
							TC = TC+1;
							matchingTrack = true;
						}
					}
				}
			}
			TC = TC/nbGTObservation;
			if(TC > maxTC)
				maxTC = TC;

			
		}
		if(matchingTrack)
			++numberOfMatchedTracks;
		TCsum+=maxTC;
		
	}
	return numberOfMatchedTracks > 0 ? (double) TCsum / (double) numberOfMatchedTracks : 0;
}

double PerfEvalOTAMetricsCalculator::getTCD()
{
	int numberOfMatchedTracks = 0;
	const auto& GTtoSysMap = mDataAssociator->getGTToSystemMap();
	std::vector<double> TCList;
	for(auto currentGtIt = GTtoSysMap.begin(); currentGtIt != GTtoSysMap.end(); ++currentGtIt)
	{
		bool matchingTrack = false;
		SceneObjectWithObservation* gtObj = currentGtIt->first;
		std::list<SceneObjectWithObservation*> currentSystemList = currentGtIt->second;

		double nbGTObservation = gtObj->mObservations.size();
		double maxTC = 0;
		for(auto currentSystemIt = currentSystemList.begin(); currentSystemIt != currentSystemList.end(); ++currentSystemIt)
		{
			double TC = 0;
			SceneObjectWithObservation* systemObj = (*currentSystemIt);
			int startOverlap, endOverlap, temporalIntersection, temporalUnion;
			if(MetricsHelpers::temporalOverlap(gtObj->mObservations, systemObj->mObservations, startOverlap, endOverlap, temporalIntersection, temporalUnion))
			{
				//std::vector<double> distList;
				for(int currentTimestamp = startOverlap; currentTimestamp <= endOverlap; ++currentTimestamp)
				{
					Observation* gtObs = MetricsHelpers::getObservation(gtObj->mObservations, currentTimestamp);
					Observation* sysObs = MetricsHelpers::getObservation(systemObj->mObservations, currentTimestamp);

					if(gtObs && sysObs)			
					{
						if(Rect::overlap(gtObs->mRectangle, sysObs->mRectangle) > mT_ov)
						{
							TC = TC+1;
							matchingTrack = true;
						}
					}
				}
			}
			TC = TC/nbGTObservation;
			if(TC > maxTC)
				maxTC = TC;
		}
		if(matchingTrack)
			TCList.push_back(maxTC);
		


	}


	//calculate average
	double TCT = 0;
	for(unsigned int i = 0; i < TCList.size(); ++i)
		TCT+=TCList[i];
	TCT = TCT/(double)TCList.size();
	double TCD = 0;
	for(unsigned int i = 0; i < TCList.size(); ++i)
		TCD += pow(TCList[i]-TCT, 2);

	TCD = sqrt(TCD/TCList.size());
	//D�finition est un peu diff�rente du paper ici. On utilise pas N-1 et on met les valeurs au carr� comme dans les autres deviation...
	return TCList.size() > 0 ? (double) TCD / (double) TCList.size() : 0;
	
}



PerfEvalOTAMetrics PerfEvalOTAMetricsCalculator::calculateMetrics()
{
	PerfEvalOTAMetrics m;
	m.nbGTTracks = getNbGTTracks();
	m.nbSystemTracks = getNbSystemTracks();
	m.CDT = getCDT();
	m.FAT = getFAT();
	m.TDF = getTDF();
	m.TF = getTF();
	m.IDC = getIDC();
	m.LT = getLT();
	m.CTM = getCTM();
	m.CTD = getCTD();
	m.TMEMT= getTMEMT();
	m.TMEMTD = getTMEMTD();
	m.TCM = getTCM();
	m.TCD = getTCD();
	return m;
}

unsigned int PerfEvalOTAMetricsCalculator::getWrongDetectionScore(const std::vector<SceneObjectWithObservation*>& detectionList, const std::vector<SceneObjectWithObservation*>& toBeDetected)
{
	unsigned int Score = 0;
	for(unsigned int j = 0; j < detectionList.size(); ++j)
	{
		SceneObjectWithObservation* systemObj = detectionList[j];
		const std::vector<Observation*>& systemObservations = systemObj->mObservations;
		bool tempOverlap = false;
		bool spatOverlap = false;
		for(unsigned int i = 0; i < toBeDetected.size(); ++i)
		{
			SceneObjectWithObservation* gtObj = toBeDetected[i];
			const std::vector<Observation*>& gtObservations = gtObj->mObservations;
			if(!gtObservations.empty())
			{	
				int startOverlap, endOverlap, temporalIntersection, temporalUnion;
				if(MetricsHelpers::temporalOverlap(gtObservations, systemObservations, startOverlap, endOverlap, temporalIntersection, temporalUnion))
				{
					if(((double)temporalIntersection)/((double)(systemObservations.size())) >= mTR_ov)
					{
						tempOverlap = true;
						double accumulator = 0;
						for(int currentTimestamp = startOverlap; currentTimestamp <= endOverlap; ++currentTimestamp)
						{
							Observation* gtObs = MetricsHelpers::getObservation(gtObservations, currentTimestamp);
							Observation* sysObs = MetricsHelpers::getObservation(systemObservations, currentTimestamp);
							if(gtObs && sysObs)							
								accumulator += MetricsHelpers::spatialOverlap(gtObs, sysObs);							
						}
						if(accumulator/((double)temporalIntersection) >= mT_ov)						
							spatOverlap = true;	
					}
				}
			}
		}
		Score += !(tempOverlap && spatOverlap);
	}
	return Score;
}
